
var http = require('http');

var server = http.createServer(function(req,res){
    res.writeHead(200);
    res.end("For getting weather info, please go to .../weather");
}).listen(process.env.PORT || 3000);


// fs = require("fs")
// request = require('request');
// http = require('http');
// path = require('path');
// async = require('async');
// express = require('express');
// router = express();
// server = http.createServer(router);

// function request_JSON(url, callback) {
//   request_func(url, function(json) {
//     callback(JSON.parse(json));
//   })
// }

// function request_func(url, callback) {
//   request(url, function(error, response, body) {
//     try {
//       callback(body)
//     }
//     catch (e) {}
//   });
// }


// function response_json(url, res) {
//   request_func(
//     url,
//     function(json) {
//       res.send(json)
//     }
//   );
// }

// function rj(u, r) {
//   response_json(u, r)
// }

// function mrj(u) {
//   return function(req, res) {
//     rj(u, res);
//   }
// }
// // progress main response json
// function pmrj(mu, u) {
//   router.get(mu, mrj(u));
// }

// // 
// // 
// // 

// function read_file_func(fn) {
//   return fs.readFileSync(fn, 'utf8');
// }
// // response file url
// function rfu(url, fn) {
//   router.get(url, function(req, res) {
//     res.send(read_file_func(fn));
//   });
// }

// // response json array url
// function rjru(url, arr) {
//   router.get(url, function(req, res) {
//     res.send(JSON.stringify(arr));
//   });
// }


// // get request customed response
// function grcr(url, callback) {
//   console.log('GET route ' + url);
//   router.get(url, function(req, res) {
//     res.send(callback(req.query));
//   });
// }
// // post request customed response
// function porcr(url, callback) {
//   console.log('POST route ' + url);
//   router.post(url, function(req, res) {
//     console.log('post response');
//     // res.status(201).send(callback(req.query));
//     res.send(callback(req.query));
//   });
// }




// grcr('/a', function(req) {
//   return 'a';
// });


// // pmrj('/I', 'http://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22');
// // pmrj('/L', 'http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22');
// // pmrj('/', 'http://samples.openweathermap.org/data/2.5/weather?lat=35&lon=139&appid=b6907d289e10d714a6e88b30761fae22');

// // rfu('/citys', 'city_list.txt');

// // rfu('/R', 'README.md');

// // rjru('/Weather', { 'message': 'This is weather page.' })
// // rjru('/BookMark', { 'message': 'This is weather page.' })

// // porcr('/BookMark/put', function(req) {
// //   return 'put ' + JSON.stringify(req);
// // });
// // porcr('/BookMark/del', function(req) {
// //   return 'del ' + JSON.stringify(req);
// // });

// // porcr('/BookMark/add', function(req) {
// //   return 'add ' + JSON.stringify(req);
// // });

// // porcr('/BookMark/edit', function(req) {
// //   return 'put ' + JSON.stringify(req);
// // });

// router.use(express.static(path.resolve(__dirname, 'client')));
// server.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function() {
//   var addr = server.address();
//   console.log(addr.address + ":" + addr.port);
// });
